FROM bellsoft/liberica-openjdk-alpine-musl:11
WORKDIR /opt/app
ENV JAVA_OPTS="-XX:+UseContainerSupport -Xmx256m -Xss512k -XX:MetaspaceSize=100m"
ENV PROFILE="-Dspring.profiles.active=prod"
ARG JAR_FILE
COPY ./target/${JAR_FILE} /opt/app/app.jar
CMD java $JAVA_OPTS $PROFILE -jar app.jar
