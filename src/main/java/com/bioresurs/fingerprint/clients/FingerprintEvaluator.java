package com.bioresurs.fingerprint.clients;

import com.bioresurs.fingerprint.models.CattleBiometric;
import com.bioresurs.fingerprint.models.dto.FingerprintDTO;
import com.machinezoo.sourceafis.FingerprintImage;
import com.machinezoo.sourceafis.FingerprintMatcher;
import com.machinezoo.sourceafis.FingerprintTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class FingerprintEvaluator {

    private static final Logger LOG = LoggerFactory.getLogger(FingerprintEvaluator.class);

    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private FingerprintMatcher fingerprintMatcher;

    @Async("threadPoolTaskExecutor")
    public CompletableFuture<FingerprintDTO> evaluate(final CattleBiometric cattleBiometric) {
        final String url = cattleBiometric.getCloudinaryImage().getUrl();
        LOG.info("Calling url for fetching image: {}", url);
        try {
            byte[] imageBytes = this.restTemplate.getForObject(url, byte[].class);
            final double score = fingerprintMatcher.match(new FingerprintTemplate(this.createFingerprintImageObject(imageBytes)));
            return CompletableFuture.completedFuture(new FingerprintDTO(cattleBiometric.getId(), cattleBiometric.getNumber(), cattleBiometric.getBreed(), score));
        } catch (Exception e) {
            LOG.error("Evaluation error: {}.\nEntity: {}", e.getMessage(), cattleBiometric);
            return CompletableFuture.failedFuture(e);
        }
    }

    @Async("threadPoolTaskExecutor")
    public CompletableFuture<FingerprintDTO> evaluateWithCache(final CattleBiometric cattleBiometric) {
        try {
            
            if(cattleBiometric.getFingerprintTemplate() == null) {
                return CompletableFuture.failedFuture(new RuntimeException("fingerprint template is null"));
            }

            final FingerprintTemplate fingerprintTemplate = new FingerprintTemplate(cattleBiometric.getFingerprintTemplate().getData());
            double score = this.fingerprintMatcher.match(fingerprintTemplate);
            return CompletableFuture.completedFuture(new FingerprintDTO(cattleBiometric.getId(), cattleBiometric.getNumber(), cattleBiometric.getBreed(), score));
        } catch (Exception e) {
            LOG.error("Evaluation error: {}.\nEntity: {}", e.getMessage(), cattleBiometric);
            return CompletableFuture.failedFuture(e);
        }
    }

    private FingerprintImage createFingerprintImageObject(final byte[] imageData) {
        final FingerprintImage fingerprintImage = new FingerprintImage();
        fingerprintImage.decode(imageData);
        return fingerprintImage;
    }
}
