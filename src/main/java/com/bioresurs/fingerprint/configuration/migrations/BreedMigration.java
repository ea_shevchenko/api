package com.bioresurs.fingerprint.configuration.migrations;

import com.bioresurs.fingerprint.models.Breed;
import com.bioresurs.fingerprint.repository.CattleBreedRepository;
import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;

import static com.bioresurs.fingerprint.configuration.migrations.MigrationConstants.BREED;

@ChangeLog
public class BreedMigration {
    
    @ChangeSet(order = "003", id = "init_cattle_breed", author = "eugene")
    public void initCattleBreed(CattleBreedRepository cattleBreedRepository) {

        final Breed existedBreed = cattleBreedRepository.findByName(BREED);

        if(existedBreed == null) {
            final Breed breed = new Breed();
            breed.setName(BREED);
            cattleBreedRepository.save(breed);   
        }
    }
}
