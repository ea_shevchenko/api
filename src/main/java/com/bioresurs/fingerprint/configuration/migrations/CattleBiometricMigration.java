package com.bioresurs.fingerprint.configuration.migrations;

import com.bioresurs.fingerprint.models.CattleBiometric;
import com.bioresurs.fingerprint.repository.CattleBiometricRepository;
import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate;
import org.bson.Document;

import static com.bioresurs.fingerprint.configuration.migrations.MigrationConstants.BREED;
import static com.bioresurs.fingerprint.configuration.migrations.MigrationConstants.NUMBER;

@ChangeLog
public class CattleBiometricMigration {
    
    @ChangeSet(order = "004", id = "init_cattle_biometric", author = "eugene")
    public void initCattleNumberField(CattleBiometricRepository cattleBiometricRepository, MongockTemplate mongockTemplate) {

//        final Document updater = new Document()
//                .append("$set", new Document().append("number", ""));
//        
//        mongockTemplate.getCollection("CattleFingerprint")
//                .updateMany(new Document(), updater);
//
//        final CattleBiometric existedCattleBiometriс = cattleBiometricRepository.findByNumber(NUMBER);
//
//        if(existedCattleBiometriс == null) {
//            CattleBiometric cattleBiometric = new CattleBiometric();
//            cattleBiometric.setBreed(BREED);
//            cattleBiometric.setCloudinaryImage(null);
//            cattleBiometric.setFingerprintTemplate(null);
//            cattleBiometricRepository.save(cattleBiometric);
//        }
    }
}
