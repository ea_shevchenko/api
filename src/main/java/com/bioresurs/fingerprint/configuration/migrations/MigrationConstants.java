package com.bioresurs.fingerprint.configuration.migrations;

public final class MigrationConstants {

    public static final String BREED = "Симентальська";
    public static final String NUMBER = "123456789";

    private MigrationConstants(){
    }
}
