package com.bioresurs.fingerprint.configuration.migrations;

import com.bioresurs.fingerprint.models.ERole;
import com.bioresurs.fingerprint.models.Role;
import com.bioresurs.fingerprint.repository.RoleRepository;
import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;

import java.util.ArrayList;
import java.util.List;

@ChangeLog
public class RolesMigration {

    @ChangeSet(order = "001", id = "init_roles", author = "eugene")
    public void initRoles(RoleRepository roleRepository) {

        List<Role> roles = new ArrayList<>();
        
        Role adminRole = new Role();
        adminRole.setName(ERole.ROLE_ADMIN);

        Role moderatorRole = new Role();
        moderatorRole.setName(ERole.ROLE_MODERATOR);

        Role userRole = new Role();
        userRole.setName(ERole.ROLE_USER);
        
        roles.add(adminRole);
        roles.add(moderatorRole);
        roles.add(userRole);
        
        roleRepository.saveAll(roles);
    }
}
