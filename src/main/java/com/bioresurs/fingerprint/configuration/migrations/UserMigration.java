package com.bioresurs.fingerprint.configuration.migrations;

import com.bioresurs.fingerprint.models.ERole;
import com.bioresurs.fingerprint.models.Role;
import com.bioresurs.fingerprint.models.User;
import com.bioresurs.fingerprint.repository.RoleRepository;
import com.bioresurs.fingerprint.repository.UserRepository;
import com.bioresurs.fingerprint.utils.AppConstants;
import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.google.common.collect.Sets;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.Set;

@ChangeLog
public class UserMigration {

    @ChangeSet(order = "002", id = "init_bioresurs_user", author = "eugene")
    public void initBioresursUser(UserRepository userRepository,
                                  RoleRepository roleRepository,
                                  PasswordEncoder passwordEncoder) {

        final User user = new User(AppConstants.APP_ADMIN_USERNAME,
                AppConstants.APP_ADMIN_EMAIL, 
                passwordEncoder.encode(AppConstants.APP_ADMIN_PASSWORD));


        final Optional<Role> optionalRole = roleRepository.findByName(ERole.ROLE_ADMIN);
        if(optionalRole.isPresent()) {
            user.setRoles(Set.of(optionalRole.get()));
            userRepository.save(user);
        }
    }
}
