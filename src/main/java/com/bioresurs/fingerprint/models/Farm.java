package com.bioresurs.fingerprint.models;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("CattleFarm")
public class Farm extends BasicModel{

    private static final long serialVersionUID = 1151032510884877286L;
    
    @Indexed(unique = true)
    private String name;
    private String location;
    
    @DBRef(lazy = true)
    private List<CattleBiometric>  cattleBiometrics = new ArrayList<>();

    public Farm() {
    }

    public Farm(final String name, final String location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<CattleBiometric> getCattleBiometrics() {
        return cattleBiometrics;
    }

    public void setCattleBiometrics(List<CattleBiometric> cattleBiometrics) {
        this.cattleBiometrics = cattleBiometrics;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Farm{");
        sb.append("name='").append(name).append('\'');
        sb.append(", location='").append(location).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
