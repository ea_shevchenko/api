package com.bioresurs.fingerprint.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

public class BasicModel implements Serializable {

    private static final long serialVersionUID = 2483500384599755850L;

    @Id
    private UUID id;

    @CreatedDate
    @JsonIgnore
    private Instant createdUTC;

    @LastModifiedDate
    @JsonIgnore
    private Instant updatedUTC;

    @JsonIgnore
    @Version
    private Integer version;

    protected BasicModel() {
    }

    protected BasicModel(final BasicModel other) {
        this.id = other.getId();
        this.createdUTC = other.getCreatedUTC();
        this.updatedUTC = other.getUpdatedUTC();
        this.version = other.getVersion();
    }

    public UUID getId() {
        return this.id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public Instant getCreatedUTC() {
        return this.createdUTC;
    }

    public void setCreatedUTC(final Instant createdUTC) {
        this.createdUTC = createdUTC;
    }

    public Instant getUpdatedUTC() {
        return this.updatedUTC;
    }

    public void setUpdatedUTC(final Instant updatedUTC) {
        this.updatedUTC = updatedUTC;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setVersion(final Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicModel that = (BasicModel) o;
        return createdUTC.equals(that.createdUTC) &&
                updatedUTC.equals(that.updatedUTC) &&
                version.equals(that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(createdUTC, updatedUTC, version);
    }
}
