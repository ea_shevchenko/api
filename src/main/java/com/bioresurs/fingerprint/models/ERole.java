package com.bioresurs.fingerprint.models;

public enum ERole {
    ROLE_USER {
        @Override
        public String toString() {
            return "ROLE_USER";
        }
    },
    ROLE_MODERATOR {
        @Override
        public String toString() {
            return "ROLE_MODERATOR";
        }
    },
    ROLE_ADMIN {
        @Override
        public String toString() {
            return "ROLE_ADMIN";
        }
    }


}
