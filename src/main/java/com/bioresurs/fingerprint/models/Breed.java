package com.bioresurs.fingerprint.models;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("CattleBreed")
public class Breed extends BasicModel {
    
    private static final long serialVersionUID = -4116736914266733823L;

    @Indexed(unique = true)
    private String name;

    public Breed() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Breed{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
