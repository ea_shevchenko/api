package com.bioresurs.fingerprint.models.dto;

import java.io.Serializable;

public class ObjectResponse<T> implements Serializable {
    
    private static final long serialVersionUID = -1022531376770448139L;
    
    private Status status;
    private T response;
}
