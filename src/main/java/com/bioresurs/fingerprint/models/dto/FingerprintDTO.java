package com.bioresurs.fingerprint.models.dto;

import com.bioresurs.fingerprint.models.CattleBiometric;

import java.io.Serializable;
import java.util.UUID;

public class FingerprintDTO implements Serializable {

    private UUID id;
    private String name;
    private String breed;
    private double match;
    private String cloudinaryUrl;

    public FingerprintDTO(final UUID id, final String name, final String breed) {
        this.id = id;
        this.name = name;
        this.breed = breed;
    }

    public FingerprintDTO(final UUID id, final String name, final String breed, final double match) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.match = match;
    }

    public FingerprintDTO(final UUID id, final String name, final String breed, final String cloudinaryUrl) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.cloudinaryUrl = cloudinaryUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public double getMatch() {
        return match;
    }

    public void setMatch(double match) {
        this.match = match;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCloudinaryUrl() {
        return cloudinaryUrl;
    }

    public void setCloudinaryUrl(String cloudinaryUrl) {
        this.cloudinaryUrl = cloudinaryUrl;
    }

    public static FingerprintDTO createCattleBiometricDTO(final CattleBiometric cattleBiometric) {
        return new FingerprintDTO(cattleBiometric.getId(), cattleBiometric.getNumber(), cattleBiometric.getBreed(), cattleBiometric.getCloudinaryImage().getUrl());
    }
}
