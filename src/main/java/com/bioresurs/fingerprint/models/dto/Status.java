package com.bioresurs.fingerprint.models.dto;

import java.io.Serializable;

public class Status implements Serializable {
    
    private static final long serialVersionUID = -3975810405443619328L;
    
    private String type;
    private String message;
    
    public enum StatusType {
        SUCCESS,
        FAIL
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
