package com.bioresurs.fingerprint.models.dto;

public class JwtResponse {
    private String token;
    private String tokenType = "Bearer";

    public JwtResponse(final String accessToken) {
        this.token = accessToken;
    }

    public String getToken() {
        return token;
    }

    public String getTokenType() {
        return tokenType;
    }
}
