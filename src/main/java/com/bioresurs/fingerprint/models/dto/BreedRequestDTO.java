package com.bioresurs.fingerprint.models.dto;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

public class BreedRequestDTO implements Serializable {
    
    private static final long serialVersionUID = 1137342906071418339L;
    
    @NotEmpty
    private String name;

    public BreedRequestDTO() {
    }

    public BreedRequestDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
