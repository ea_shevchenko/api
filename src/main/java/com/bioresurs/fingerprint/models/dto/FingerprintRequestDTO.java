package com.bioresurs.fingerprint.models.dto;

import java.io.Serializable;

public class FingerprintRequestDTO implements Serializable {
    
    private static final long serialVersionUID = -7619015011064708063L;
    
    private String farmName;
    
    private String number;
    
    private String name;
    
    private String breed;

    public FingerprintRequestDTO() {
    }

    public FingerprintRequestDTO(String farmName, String number, String name, String breed) {
        this.farmName = farmName;
        this.number = number;
        this.name = name;
        this.breed = breed;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}
