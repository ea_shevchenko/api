package com.bioresurs.fingerprint.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.Binary;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("CattleFingerprint")
public class CattleBiometric extends BasicModel {

    private static final long serialVersionUID = -5595898076628331623L;

    @Indexed(unique = true)
    private String number;
    private String name;
    @Indexed(unique = true)
    private String breed;
    @Indexed(unique = true)
    private CloudinaryImage cloudinaryImage;
    private Binary fingerprintTemplate;

    public CattleBiometric() {
    }

    public CattleBiometric(final String number,
                           final String name,
                           final String breed,
                           final CloudinaryImage cloudinaryImage,
                           final Binary fingerprintTemplate) {
        this.number = number;
        this.name = name;
        this.breed = breed;
        this.cloudinaryImage = cloudinaryImage;
        this.fingerprintTemplate = fingerprintTemplate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public CloudinaryImage getCloudinaryImage() {
        return cloudinaryImage;
    }

    public void setCloudinaryImage(CloudinaryImage cloudinaryImage) {
        this.cloudinaryImage = cloudinaryImage;
    }

    @JsonIgnore
    public Binary getFingerprintTemplate() {
        return fingerprintTemplate;
    }

    public void setFingerprintTemplate(Binary fingerprintTemplate) {
        this.fingerprintTemplate = fingerprintTemplate;
    }

    @Override
    public String toString() {
        return "CattleBiometric{" +
                "id='" + super.getId() + '\'' +
                "name='" + number + '\'' +
                ", breed='" + breed + '\'' +
                ", cloudinaryImage=" + cloudinaryImage +
                '}';
    }
}
