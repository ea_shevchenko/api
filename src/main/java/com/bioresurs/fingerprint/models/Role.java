package com.bioresurs.fingerprint.models;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Roles")
public class Role extends BasicModel {

    private static final long serialVersionUID = -1668996830780591670L;
    
    private ERole name;

    public Role() {

    }

    public Role(ERole name) {
        this.name = name;
    }

    public ERole getName() {
        return name;
    }

    public void setName(ERole name) {
        this.name = name;
    }
}
