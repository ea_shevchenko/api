package com.bioresurs.fingerprint.utils;

public final class AppConstants {
    
    private AppConstants(){
    }
    
    public static final String APP_ADMIN_USERNAME = "bioresurs";
    public static final String APP_ADMIN_EMAIL = "bioresurs.ck.ua@gmail.com";
    public static final String APP_ADMIN_PASSWORD = "bioresurs";
}
