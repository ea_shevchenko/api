package com.bioresurs.fingerprint.utils;

import com.bioresurs.fingerprint.models.BasicModel;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;

import java.util.UUID;

public class MongoUuidGenerator extends AbstractMongoEventListener<BasicModel> {
    @Override
    public void onBeforeConvert(final BeforeConvertEvent<BasicModel> event) {
        final BasicModel model = event.getSource();
        if (model.getId() == null) {
            model.setId(UUID.randomUUID());
        }
        super.onBeforeConvert(event);
    }
}
