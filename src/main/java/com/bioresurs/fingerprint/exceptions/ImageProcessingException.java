package com.bioresurs.fingerprint.exceptions;

public class ImageProcessingException extends ServiceException {

    public ImageProcessingException(String message) {
        super(message);
    }

    public ImageProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
