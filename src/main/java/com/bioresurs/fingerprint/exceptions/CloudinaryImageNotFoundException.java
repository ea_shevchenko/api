package com.bioresurs.fingerprint.exceptions;

public class CloudinaryImageNotFoundException extends ServiceRuntimeException {
    
    public CloudinaryImageNotFoundException(String message) {
        super(message);
    }
}
