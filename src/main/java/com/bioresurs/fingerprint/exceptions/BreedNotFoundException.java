package com.bioresurs.fingerprint.exceptions;

public class BreedNotFoundException extends ServiceRuntimeException {
    
    public BreedNotFoundException(String message) {
        super(message);
    }
}
