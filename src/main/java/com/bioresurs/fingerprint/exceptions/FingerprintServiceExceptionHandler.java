package com.bioresurs.fingerprint.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class FingerprintServiceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { CloudinaryImageNotFoundException.class })
    protected ResponseEntity<Object> handleCloudinaryImageNotFoundException(final CloudinaryImageNotFoundException ex, final WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = { BreedNotFoundException.class })
    protected ResponseEntity<Object> handleBreedNotFoundException(final BreedNotFoundException ex, final WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
