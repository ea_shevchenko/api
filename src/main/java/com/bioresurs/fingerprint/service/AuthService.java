package com.bioresurs.fingerprint.service;

import com.bioresurs.fingerprint.models.dto.SignupRequest;

public interface AuthService {
    
    public void registerUser(SignupRequest signupRequest);
}
