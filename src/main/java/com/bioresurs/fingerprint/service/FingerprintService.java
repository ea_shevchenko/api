package com.bioresurs.fingerprint.service;

import com.bioresurs.fingerprint.exceptions.BreedNotFoundException;
import com.bioresurs.fingerprint.models.CattleBiometric;
import com.bioresurs.fingerprint.models.dto.FingerprintDTO;
import com.bioresurs.fingerprint.models.dto.FingerprintRequestDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface FingerprintService {
    
    CattleBiometric addCachedProbe(FingerprintRequestDTO fingerprintRequestDTO, MultipartFile file) throws IOException, BreedNotFoundException;
    
    List<FingerprintDTO> getFingerprintModels() throws JsonProcessingException;

    List<FingerprintDTO> evaluate(MultipartFile file) throws IOException;

    FingerprintDTO evaluateSingle(MultipartFile file) throws IOException;

    FingerprintDTO evaluateSingleWithCache(MultipartFile file) throws IOException;

    void deleteFingerprint(UUID id);
}
