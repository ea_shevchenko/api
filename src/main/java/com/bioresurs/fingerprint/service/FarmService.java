package com.bioresurs.fingerprint.service;

import com.bioresurs.fingerprint.models.Farm;
import com.bioresurs.fingerprint.models.dto.FingerprintDTO;

import java.util.List;
import java.util.UUID;

public interface FarmService {
    
    List<Farm> getFarms();
    
    List<FingerprintDTO> getCattleFingerprintsByFarm(String name);
    
    Farm addFarm(String name, String location);

    void deleteFarm(UUID id);
}
