package com.bioresurs.fingerprint.service.impl;

import com.bioresurs.fingerprint.models.Breed;
import com.bioresurs.fingerprint.models.Farm;
import com.bioresurs.fingerprint.repository.CattleBreedRepository;
import com.bioresurs.fingerprint.service.BreedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BreedServiceImpl implements BreedService {

    @Autowired
    private CattleBreedRepository cattleBreedRepository;

    @Override
    public List<Breed> getBreeds() {
        return cattleBreedRepository.findAll();
    }

    @Override
    public Breed addBreed(final String name) {
        final Breed breed = new Breed();
        breed.setName(name);
        return cattleBreedRepository.save(breed);
    }

    @Override
    public void deleteBreed(final UUID id) {
        final Optional<Breed> optionalBreed = this.cattleBreedRepository.findById(id);
        if (optionalBreed.isPresent()) {
            final Breed breed = optionalBreed.get();
            this.cattleBreedRepository.delete(breed);
        }
    }
}
