package com.bioresurs.fingerprint.service.impl;

import com.bioresurs.fingerprint.models.CloudinaryImage;
import com.bioresurs.fingerprint.service.CloudinaryService;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Service
public class CloudinaryServiceImpl implements CloudinaryService {

    private static final Logger LOG = LoggerFactory.getLogger(CloudinaryServiceImpl.class);

    @Autowired
    private Cloudinary cloudinary;

    @Qualifier("uploadParams")
    @Autowired
    private Map<String, String> uploadParams;

    @Override
    public CloudinaryImage uploadImage(MultipartFile file) {
        try {
            final Map uploadResult = this.cloudinary.uploader().upload(file.getBytes(), this.uploadParams);
            return new CloudinaryImage(uploadResult.get("url").toString(), uploadResult.get("public_id").toString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void removeImage(String imageId) throws IOException {
        this.cloudinary.uploader().destroy(imageId, ObjectUtils.emptyMap());
        LOG.info("Image removed with public id {}", imageId);
    }
}