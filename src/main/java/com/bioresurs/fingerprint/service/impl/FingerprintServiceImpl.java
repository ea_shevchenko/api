package com.bioresurs.fingerprint.service.impl;

import com.bioresurs.fingerprint.clients.FingerprintEvaluator;
import com.bioresurs.fingerprint.exceptions.BreedNotFoundException;
import com.bioresurs.fingerprint.models.Breed;
import com.bioresurs.fingerprint.models.CattleBiometric;
import com.bioresurs.fingerprint.models.CloudinaryImage;
import com.bioresurs.fingerprint.models.Farm;
import com.bioresurs.fingerprint.models.dto.FingerprintDTO;
import com.bioresurs.fingerprint.models.dto.FingerprintRequestDTO;
import com.bioresurs.fingerprint.repository.CattleBiometricRepository;
import com.bioresurs.fingerprint.repository.CattleBreedRepository;
import com.bioresurs.fingerprint.repository.CattleFarmRepository;
import com.bioresurs.fingerprint.service.CloudinaryService;
import com.bioresurs.fingerprint.service.FingerprintService;
import com.machinezoo.sourceafis.FingerprintImage;
import com.machinezoo.sourceafis.FingerprintMatcher;
import com.machinezoo.sourceafis.FingerprintTemplate;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class FingerprintServiceImpl implements FingerprintService {

    private static final Logger logger = LoggerFactory.getLogger(FingerprintServiceImpl.class);
    

    @Autowired
    private CattleBiometricRepository cattleBiometricRepository;
    
    @Autowired
    private CattleFarmRepository cattleFarmRepository;
    
    @Autowired
    private CattleBreedRepository cattleBreedRepository;

    @Autowired
    private CloudinaryService cloudinaryService;

    @Autowired
    private FingerprintEvaluator fingerprintEvaluator;
    
    @Autowired
    private FingerprintMatcher fingerprintMatcher;

    @Override
    public CattleBiometric addCachedProbe(final FingerprintRequestDTO fingerprintRequestDTO, final MultipartFile file)
            throws IOException, BreedNotFoundException {
        final Breed breed = this.cattleBreedRepository.findByName(fingerprintRequestDTO.getBreed());

        if(breed == null) {
            final String message = String.format("Breed with name %s was not found", fingerprintRequestDTO.getBreed());
            logger.error(message);
            throw new BreedNotFoundException(message);
        }
        
        final FingerprintImage fingerprintImage = createFingerprintImageObject(file);
        final FingerprintTemplate fingerprintTemplate = new FingerprintTemplate(fingerprintImage);
        final CloudinaryImage cloudinaryImage = this.cloudinaryService.uploadImage(file);
        final CattleBiometric savedCattleBiometric =
                this.cattleBiometricRepository.save(
                        new CattleBiometric(
                                fingerprintRequestDTO.getNumber(),
                                fingerprintRequestDTO.getName(),
                                fingerprintRequestDTO.getBreed(),
                                cloudinaryImage,
                                new Binary(fingerprintTemplate.toByteArray())));
        if(fingerprintRequestDTO.getFarmName() != null) {
            final Farm farm = cattleFarmRepository.findByName(fingerprintRequestDTO.getFarmName());
            if(farm != null) {
                farm.getCattleBiometrics().add(savedCattleBiometric);
                this.cattleFarmRepository.save(farm);
            }
        }
 
        logger.info("CattleBiometric has been saved: {}", savedCattleBiometric);
        return savedCattleBiometric;
    }

    @Override
    public List<FingerprintDTO> getFingerprintModels() {
        return this.cattleBiometricRepository.findAll().stream()
                .map(FingerprintDTO::createCattleBiometricDTO).collect(Collectors.toList());
    }

    @Override
    public List<FingerprintDTO> evaluate(final MultipartFile file) throws IOException {
        return processEvaluation(file, false);
    }

    @Override
    public FingerprintDTO evaluateSingle(final MultipartFile file) throws IOException {
        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final FingerprintDTO fingerprintDTO = 
                Collections.max(this.processEvaluation(file, false), Comparator.comparing(FingerprintDTO::getMatch));
        stopWatch.stop();
        logger.info("EvaluateSingle. Elapsed Time in ms: {} ", stopWatch.getTotalTimeMillis());
        return fingerprintDTO;
    }

    @Override
    public FingerprintDTO evaluateSingleWithCache(MultipartFile file) throws IOException {
        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final FingerprintDTO fingerprintDTO =
                Collections.max(this.processEvaluation(file, true), Comparator.comparing(FingerprintDTO::getMatch));
        stopWatch.stop();
        logger.info("EvaluateSingleWithCache cache Elapsed Time in ms: {} ", stopWatch.getTotalTimeMillis());
        return fingerprintDTO;
    }

    @Override
    public void deleteFingerprint(UUID id) {
        final Optional<CattleBiometric> cattleBiometric =
                this.cattleBiometricRepository.findById(id);
        try {
            if (cattleBiometric.isPresent()) {
                CattleBiometric cattleBiometricForRemove = cattleBiometric.get();
                if (cattleBiometricForRemove.getCloudinaryImage() != null) {
                    final CloudinaryImage imageForRemove = cattleBiometricForRemove.getCloudinaryImage();
                    this.cloudinaryService.removeImage(imageForRemove.getPublicId());
                    this.cattleBiometricRepository.deleteById(id);
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private List<FingerprintDTO> processEvaluation(final MultipartFile file, final boolean useCache) throws IOException {
        this.fingerprintMatcher.index(new FingerprintTemplate(createFingerprintImageObject(file)));
        final List<CompletableFuture<FingerprintDTO>> completableFutures = new ArrayList<>();
        
        if(useCache) {
            this.cattleBiometricRepository.findAll().forEach(cattleBiometric ->
                    completableFutures.add(this.fingerprintEvaluator.evaluateWithCache(cattleBiometric)));   
        } else {
            this.cattleBiometricRepository.findAll().forEach(cattleBiometric ->
                    completableFutures.add(this.fingerprintEvaluator.evaluate(cattleBiometric))); 
        }

        final List<FingerprintDTO> evaluatedResults =
                completableFutures.stream()
                        .filter(completableFuture -> !completableFuture.isCompletedExceptionally())
                        .map(CompletableFuture::join).collect(Collectors.toList());

        logger.info("FingerprintImage {} has been evaluated:", file.getOriginalFilename());
        return evaluatedResults;
    }

    private FingerprintImage createFingerprintImageObject(MultipartFile file) throws IOException {
        final FingerprintImage fingerprintImage = new FingerprintImage();
        fingerprintImage.decode(file.getBytes());
        return fingerprintImage;
    }
}
