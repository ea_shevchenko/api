package com.bioresurs.fingerprint.service.impl;

import com.bioresurs.fingerprint.models.Farm;
import com.bioresurs.fingerprint.models.dto.FingerprintDTO;
import com.bioresurs.fingerprint.repository.CattleFarmRepository;
import com.bioresurs.fingerprint.service.FarmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FarmServiceImpl implements FarmService {

    private static final Logger LOG = LoggerFactory.getLogger(FarmServiceImpl.class);


    @Autowired
    private CattleFarmRepository cattleFarmRepository;

    @Override
    public List<Farm> getFarms() {
        return cattleFarmRepository.findAll();
    }

    @Override
    public List<FingerprintDTO> getCattleFingerprintsByFarm(final String name) {
        final Farm farm = this.cattleFarmRepository.findByName(name);
        if (farm != null) {
            return farm.getCattleBiometrics()
                    .stream().map(FingerprintDTO::createCattleBiometricDTO)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public Farm addFarm(String name, String location) {
        final Farm farm = new Farm();
        farm.setName(name);
        farm.setLocation(location);
        return this.cattleFarmRepository.save(farm);
    }

    @Override
    public void deleteFarm(final UUID id) {
        final Optional<Farm> optionalFarm = this.cattleFarmRepository.findById(id);
        if (optionalFarm.isPresent()) {
            final Farm farm = optionalFarm.get();
            this.cattleFarmRepository.delete(farm);
        }
    }
}
