package com.bioresurs.fingerprint.service;

import com.bioresurs.fingerprint.models.Breed;

import java.util.List;
import java.util.UUID;

public interface BreedService {
    
    List<Breed> getBreeds();
    
    Breed addBreed(String name);

    void deleteBreed(UUID id);
}
