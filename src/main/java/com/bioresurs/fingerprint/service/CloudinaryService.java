package com.bioresurs.fingerprint.service;

import com.bioresurs.fingerprint.models.CloudinaryImage;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface CloudinaryService {
    
    CloudinaryImage uploadImage(MultipartFile file);
    
    void removeImage(String imageId) throws IOException;
}
