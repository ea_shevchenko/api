package com.bioresurs.fingerprint;

import com.bioresurs.fingerprint.exceptions.ErrorHandler;
import com.bioresurs.fingerprint.utils.MongoUuidGenerator;
import com.github.cloudyrock.spring.v5.EnableMongock;
import com.machinezoo.sourceafis.FingerprintMatcher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;
import java.util.concurrent.Executor;

@SpringBootApplication
@EnableMongock
@EnableAsync
public class CattleFingerprint {

    @Bean
    public MongoUuidGenerator mongoUuidGenerator() {
        return new MongoUuidGenerator();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }

    @Bean
    public RestTemplate restTemplate(List<HttpMessageConverter<?>> messageConverters) {
        RestTemplate restTemplate = new RestTemplate(messageConverters);
        restTemplate.setErrorHandler(new ErrorHandler());
        return restTemplate;
    }

    @Bean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        return new ByteArrayHttpMessageConverter();
    }

    @Bean(name = "threadPoolTaskExecutor")
    public Executor threadPoolTaskExecutor() {
        final ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        final int cores = Runtime.getRuntime().availableProcessors();
        final int capacity = cores * 10;
        threadPoolTaskExecutor.setThreadGroupName("fingerprint-group");
        threadPoolTaskExecutor.setThreadNamePrefix("fingerprint-evaluator");
        threadPoolTaskExecutor.setCorePoolSize(cores);
        threadPoolTaskExecutor.setMaxPoolSize(cores * 2);
        threadPoolTaskExecutor.setQueueCapacity(capacity);
        return new ThreadPoolTaskExecutor();
    }

    @Bean
    public FingerprintMatcher fingerprintMatcher() {
        return new FingerprintMatcher();
    }

    public static void main(String[] args) {
        SpringApplication.run(CattleFingerprint.class, args);
    }
}
