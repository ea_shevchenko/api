package com.bioresurs.fingerprint.helpers;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
public class FileHelper {

    public File convertMultiPartToFile(MultipartFile file) throws IOException {

        if (file.getOriginalFilename() == null) throw new RuntimeException("file is null");

        File convFile = new File(file.getOriginalFilename());
        file.transferTo(convFile);
        return convFile;
    }
}