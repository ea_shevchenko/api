package com.bioresurs.fingerprint.repository;

import java.util.Optional;
import java.util.UUID;

import com.bioresurs.fingerprint.models.ERole;
import com.bioresurs.fingerprint.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role, UUID> {
    Optional<Role> findByName(ERole name);
}
