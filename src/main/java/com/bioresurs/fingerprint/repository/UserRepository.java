package com.bioresurs.fingerprint.repository;

import java.util.Optional;
import java.util.UUID;

import com.bioresurs.fingerprint.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, UUID> {
    Optional<User> findByUsername(String username);
    
    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
