package com.bioresurs.fingerprint.repository;

import com.bioresurs.fingerprint.models.CattleBiometric;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.UUID;

public interface CattleBiometricRepository extends MongoRepository<CattleBiometric, UUID> {

    List<CattleBiometric> findAll();
    
    CattleBiometric findByNumber(String number);
}
