package com.bioresurs.fingerprint.repository;

import com.bioresurs.fingerprint.models.Farm;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.UUID;

public interface CattleFarmRepository extends MongoRepository<Farm, UUID> {

    List<Farm> findAll();

    @Query(value = "{'name': {$regex : ?0, $options: 'i'}}")
    Farm findByName(String name);
}
