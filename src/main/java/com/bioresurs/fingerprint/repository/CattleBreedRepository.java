package com.bioresurs.fingerprint.repository;

import com.bioresurs.fingerprint.models.Breed;
import com.bioresurs.fingerprint.models.Farm;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.UUID;

public interface CattleBreedRepository extends MongoRepository<Breed, UUID> {

    List<Breed> findAll();
    
    Breed findByName(String name);
}
