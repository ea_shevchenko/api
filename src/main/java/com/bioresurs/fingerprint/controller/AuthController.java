package com.bioresurs.fingerprint.controller;

import com.bioresurs.fingerprint.models.User;
import com.bioresurs.fingerprint.models.dto.JwtResponse;
import com.bioresurs.fingerprint.models.dto.LoginRequest;
import com.bioresurs.fingerprint.models.dto.MessageResponse;
import com.bioresurs.fingerprint.models.dto.SignupRequest;
import com.bioresurs.fingerprint.repository.UserRepository;
import com.bioresurs.fingerprint.security.jwt.JwtUtils;
import com.bioresurs.fingerprint.service.AuthService;
import com.bioresurs.fingerprint.utils.AppConstants;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Tag(name = "Auth", description = "Auth controller for retrieving JWT")
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private AuthService authService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String jwt = jwtUtils.generateJwtToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }
    
    @DeleteMapping()
    public ResponseEntity<?> deleteUser(@RequestParam("username") String username) {
        final Optional<User> optionalUser = userRepository.findByUsername(username);

        if (optionalUser.isPresent()) {
            final User user = optionalUser.get();
            if(user.getUsername().equals(AppConstants.APP_ADMIN_USERNAME)) {
                return ResponseEntity.unprocessableEntity().build();
            }

            userRepository.delete(user);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }
        
        authService.registerUser(signUpRequest);
        
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}
