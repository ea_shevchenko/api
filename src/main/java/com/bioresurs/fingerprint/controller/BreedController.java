package com.bioresurs.fingerprint.controller;

import com.bioresurs.fingerprint.models.dto.BreedRequestDTO;
import com.bioresurs.fingerprint.service.BreedService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Tag(name = "Breed", description = "Breed controller for the cattle")
@RestController
@RequestMapping("/api/breed")
public class BreedController implements SecuredRestController {

    private final Logger logger = LoggerFactory.getLogger(BreedController.class);

    @Autowired
    private BreedService breedService;

    @Secured({"ROLE_ADMIN"})
    @GetMapping
    public ResponseEntity<?> getBreeds() {
        logger.info("Retrieving cattle breeds...");
        return ResponseEntity.ok(this.breedService.getBreeds());
    }

    @Secured({"ROLE_ADMIN"})
    @PostMapping()
    public ResponseEntity<?> addBreed(@RequestBody @Valid final BreedRequestDTO breedRequestDTO) {
        logger.info("Creating new cattle breed with name {}", breedRequestDTO.getName());
        return ResponseEntity.ok(this.breedService.addBreed(breedRequestDTO.getName()));
    }

    @Secured({"ROLE_ADMIN"})
    @DeleteMapping
    public void deleteBreed(@RequestParam("id") final UUID id) {
        logger.info("Deleting cattle breed by id: {}", id);
        this.breedService.deleteBreed(id);
    }
}
