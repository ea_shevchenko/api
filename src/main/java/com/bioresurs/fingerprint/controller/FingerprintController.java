package com.bioresurs.fingerprint.controller;

import com.bioresurs.fingerprint.exceptions.BreedNotFoundException;
import com.bioresurs.fingerprint.models.dto.FingerprintDTO;
import com.bioresurs.fingerprint.models.dto.FingerprintRequestDTO;
import com.bioresurs.fingerprint.service.FingerprintService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Tag(name = "Fingerprint", description = "Fingerprint controller")
@RestController()
@RequestMapping("/api/fingerprints")
public class FingerprintController implements SecuredRestController {

    private final Logger logger = LoggerFactory.getLogger(FingerprintController.class);

    @Autowired
    private FingerprintService fingerprintService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping
    public ResponseEntity<?> getFingerprints() throws JsonProcessingException {
        logger.info("Retrieving fingerprints...");
        final List<FingerprintDTO> fingerprintModels = fingerprintService.getFingerprintModels();
        return ResponseEntity.ok(fingerprintModels);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> addFingerprint(@RequestPart("number") final String number,
                                            @RequestPart("name") final String name,
                                            @RequestPart("breed") final String breed,
                                            @RequestPart("farmName") final String farmName,
                                            @RequestPart("image") final MultipartFile file) 
            throws IOException, BreedNotFoundException {
        final FingerprintRequestDTO fingerprintRequestDTO = new FingerprintRequestDTO();
        fingerprintRequestDTO.setNumber(number);
        fingerprintRequestDTO.setBreed(breed);
        fingerprintRequestDTO.setFarmName(farmName);
        fingerprintRequestDTO.setName(name);
        logger.info("Adding new fingerprint: {}", fingerprintRequestDTO);
        return ResponseEntity.ok(this.fingerprintService.addCachedProbe(fingerprintRequestDTO, file));
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping(value = "evaluate", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<FingerprintDTO> evaluate(@RequestPart("image") final MultipartFile file) throws IOException {
        logger.info("Evaluating fingerprints by file: {}", file.getOriginalFilename());
        return this.fingerprintService.evaluate(file);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping(value = "evaluate-single", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public FingerprintDTO evaluateSingle(@RequestPart("image") final MultipartFile file) throws IOException {
        logger.info("Evaluating fingerprint by file: {}", file.getOriginalFilename());
        return this.fingerprintService.evaluateSingle(file);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping(value = "evaluate-single/cache", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public FingerprintDTO evaluateSingleWithCache(@RequestPart("image") final MultipartFile file) throws IOException {
        logger.info("Evaluating fingerprint by file {} with cache", file.getOriginalFilename());
        return this.fingerprintService.evaluateSingleWithCache(file);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @DeleteMapping
    public void deleteFingerprint(@RequestParam("id") final UUID id) {
        logger.info("Deleting fingerprint by id {}", id);
        this.fingerprintService.deleteFingerprint(id);
    }
}
