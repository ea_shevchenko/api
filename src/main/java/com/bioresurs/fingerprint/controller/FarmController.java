package com.bioresurs.fingerprint.controller;

import com.bioresurs.fingerprint.models.Farm;
import com.bioresurs.fingerprint.models.dto.FingerprintDTO;
import com.bioresurs.fingerprint.service.FarmService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Tag(name = "Farm", description = "Farm controller for the farms")
@RestController
@RequestMapping("/api/farm")
public class FarmController implements SecuredRestController {

    private final Logger logger = LoggerFactory.getLogger(FarmController.class);
    
    @Autowired
    private FarmService farmService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping
    public ResponseEntity<?> getFarms() {
        logger.info("Retrieving cattle farms...");
        final List<Farm> farms = farmService.getFarms();
        return ResponseEntity.ok(farms);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("fingerprints")
    public ResponseEntity<?> getFingerprintsByFarm(@RequestParam("name") final String name) {
        logger.info("Retrieving cattle farm by name: {}", name);
        final List<FingerprintDTO> fingerprintDTO = farmService.getCattleFingerprintsByFarm(name);
        return ResponseEntity.ok(fingerprintDTO);
    }

    @Secured({"ROLE_ADMIN"})
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> addFarm(@RequestPart("name") final String name,
                                     @RequestPart("location") final String location) {
        logger.info("Creating new cattle farm with name {} and location {}", name, location);
        return ResponseEntity.ok(this.farmService.addFarm(name, location));
    }

    @Secured({"ROLE_ADMIN"})
    @DeleteMapping
    public void deleteFarm(@RequestParam("id") final UUID id) {
        logger.info("Deleting cattle farm by id: {}", id);
        this.farmService.deleteFarm(id);
    }
}
